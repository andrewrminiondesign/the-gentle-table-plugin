(function( $, factory ) {

    var PsTitle = factory( $ );

    ps_observer.add_action('postbox_init', function( postbox ) {
        var inst = new PsTitle( postbox );
    }, 10, 1 );

})( jQuery, function( $ ) {

/**
 * Postbox location addon.
 */
function PsPostboxTitle() {
    this.__constructor.apply( this, arguments );
}

PsPostboxTitle.prototype = {

    /**
     * Initialize postbox with title functionality.
     * @param {PsPostbox} postbox Postbox instance in which mood functionality will be attached to.
     */
    __constructor: function( postbox ) {
        this.postbox = postbox;

        // filters and actions
        postbox.add_filter('data', this.filterData, 10, 1, this );
    },

    /**
     * Attach selected mood into post data.
     * @param {object} data
     */
    filterData: function( data ) {
        data.post_title = this.value;
        return data;
    }
};

return PsPostboxTitle;

});


////////////////////////////////////////////////////////////////////////////////////////////////////
// PsTitle (legacy)
////////////////////////////////////////////////////////////////////////////////////////////////////

(function( $, peepso, factory ) {

    factory( $, peepso );

})( jQuery || $, peepso, function( $, peepso ) {

/**
* Javascript code to handle title events
*/
function PsTitle() {
    this.$postbox = null;
    this.can_submit = false;
}

/**
 * Defines the postbox this instance is running on.
 * Called on postbox.js _load_addons()
 * @param {object} postbox This refers to the parent postbox object which this plugin may inherit, override, and manipulate its input boxes and behavior
 */
PsTitle.prototype.set_postbox = function(postbox) {
    this.$postbox = postbox;
};

/**
 * Initializes this instance's container and selector reference to a postbox instance.
 * Called on postbox.js _load_addons()
 */
PsTitle.prototype.init = function() {
    if (_.isUndefined(this.$postbox))
        return;

    var _self = this;

    this.$title = jQuery("input[name='post_title']", this.$postbox);

    // This handles adding the selected mood to the postbox_req variable before submitting to server
    ps_observer.add_filter("postbox_req_" + this.$postbox.guid, function(req) {
        return _self.set_title(req);
    }, 10, 1);
};

/**
 * Adds the selected title to the postbox_req variable
 * @param {object} req postbox request
 * @return {object} req Returns modified request with title value
 */
PsTitle.prototype.set_title = function(req) {
    if ("undefined" === typeof(req.post_title))
        req.post_title = "";

    req.post_title = jQuery("input[name='post_title']", this.$postbox).val();
    return (req);
};

/**
 * Adds a new PsTitle object to the PostBox instance.
 * @param {array} addons An array of addons that are being plugged in to the PostBox.
 */
ps_observer.add_filter('peepso_postbox_addons', function(addons) {
    addons.push(new PsTitle);
    return addons;
}, 10, 1);

});
