<?php
/**
 * Plugin Name: The Gentle Table
 * Plugin URI: https://thegentletable.com
 * Description: Handles custom features
 * Version: 1.0.0
 * Author: AndrewRMinion Design
 * Author URI: https://andrewrminion.com/
*/

/**
 * Exit if accessed directly
 */
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Add note about anonymous username on registration form
 * @param  array $fields form fields
 * @return array form fields
 */
function tgt_username_description( $fields ) {
    $fields['username']['descript'] = 'Enter your desired username; please keep it anonymous';
    return $fields;
}
add_filter( 'peepso_register_form_fields', 'tgt_username_description' );

/*
 * Modify PeepSo behavior
 */
class PeepSoGentleTableMods {

    private static $_instance = NULL;

    const PLUGIN_VERSION = '1.0.0';
    const META_POST_TITLE = '_peepso_post_title';

    /**
     * Initialize all variables, filters and actions
     */
    private function __construct() {
        add_action('peepso_init', array(&$this, 'init'));
    }

    /*
     * Return singleton instance of plugin
     */
    public static function get_instance() {
        if ( NULL === self::$_instance) {
            self::$_instance = new self();
        }

        return (self::$_instance);
    }

    /*
     * Initialize this plugin
     */
    public function init() {
        if ( ! is_admin() ) {
            add_action( 'wp_enqueue_scripts', array( &$this, 'enqueue_scripts') );
            add_action( 'wp_insert_post', array( &$this, 'save_title' ), 100 );
            add_action( 'peepso_activity_after_save_post', array( &$this, 'save_title' ), 100 );
            add_action( 'peepso_postbox_before', array( &$this, 'add_postbox_title' ), 5 );
            add_filter( 'peepso_activity_allow_empty_content', array( &$this, 'filter_activity_allow_empty_content' ), 10, 1 );
            add_filter( 'peepso_activity_content_before', array( &$this, 'filter_content_before' ), 10, 1 );
            add_filter( 'peepso_activity_post_edit', array( &$this, 'filter_post_edit' ), 10, 1 );
        }
    }

    /**
     * Load required styles and scripts
     */
    public function enqueue_scripts() {
        wp_enqueue_script( 'peepso-title', plugin_dir_url( __FILE__ ) . 'assets/js/postbox-title.min.js', array( 'peepso', 'peepso-postbox' ), self::PLUGIN_VERSION, TRUE );
    }

    /**
     * This function inserts title input on the post box
     * @param array $out_html is the formated html code that get inserted in the postbox
     */
    public function add_postbox_title() { ?>
            <div class="ps-postbox-title">
                <div style="position:relative">
                    <div class="ps-postbox-input ps-inputbox">
                        <input class="ps-input ps-postbox-input" placeholder="Post Title" name="post_title" />
                    </div>
                </div>
            </div>
        <?php
    }

    /**
     * This function saves the title data for the post
     * @param $post_id is the ID assign to the posted content
     */
    public function save_title( $post_id ) {
        $input = new PeepSoInput();
        $title = $input->val( 'post_title' );

        if ( empty( $title ) ) {
            delete_post_meta( $post_id, self::META_POST_TITLE );
        } else {
            update_post_meta( $post_id, self::META_POST_TITLE, $title );
        }
    }

    /**
     * Add title before post content
     * @param  string $content HTML post content
     * @return string HTML post content
     */
    public function filter_content_before( $content ) {
        global $post;
        $post_title = get_post_meta( $post->ID, self:: META_POST_TITLE, TRUE );

        if ( ! empty( $post_title ) ) {
            $content = '<h3 class="post-title">'.  $post_title . '</h3>' . $content;
        }

        return $content;
    }

    /**
     * Allows empty post content if a title is set
     * @param boolean $allowed Current state of the allow posting check
     * @return boolean returns TRUE when a title is present to indicate that a post with not content and a title is publishable
     */
    public function filter_activity_allow_empty_content( $allowed ) {
        $input = new PeepSoInput();
        $title = $input->val( 'post_title' );
        if ( ! empty( $title ) ) {
            $allowed = TRUE;
        }

        return ( $allowed );
    }

    /**
     * Add post title to data sent to frontend when editing a post
     * @param  array [$data            = array()] post data
     * @return array post data
     */
    public function filter_post_edit( $data = array() ) {
        $input = new PeepSoInput();
        $post_id = $data['post_id'];

        $title = get_post_meta( $post_id, self::META_POST_TITLE, TRUE );
        if ( ! empty( $title ) ) {
            $data['post_title'] = $title;
        }

        return $data;
    }

}

PeepSoGentleTableMods::get_instance();

/**
 * Allow public sharing of BadgeOS submissions
 * @param  string $sub_form HTML form content
 * @return string HTML form content
 */
function tgt_badgeos_submission_form( $sub_form ) {
    $checkbox = '
    <p class="public-share"><label for="tgt_share_publicly"><input type="checkbox" name="tgt_share_publicly" id="tgt_share_publicly" /> Share this post publicly on the Activity feed?</label></p>
    ';

    $sub_form = str_replace( '<p class="badgeos-submission-submit">', $checkbox . '<p class="badgeos-submission-submit">', $sub_form );
    return $sub_form;
}
add_filter( 'badgeos_get_submission_form', 'tgt_badgeos_submission_form' );

/**
 * Post BadgeOS submission to PeepSo activity stream if indicated by user
 * @param integer $submission_id submission post ID
 * @param array   $post_data     $_POST data from frontend form
 */
function tgt_badgeos_post_submission( $submission_id, $post_data ) {
    if ( isset( $post_data['tgt_share_publicly'] ) && $post_data['tgt_share_publicly'] === 'on' ) {
        $input = new PeepSoInput();
        $user_id = $input->int('id');

        $peepso_activity = PeepSoActivity::get_instance();
        $post_id = $peepso_activity->add_post($user_id, $user_id, get_post( $submission_id )->post_content);
    }
}
add_action( 'badgeos_save_submission', 'tgt_badgeos_post_submission', 10, 2 );

/**
 * Change “Badges” in page title
 * @param  string $title page title
 * @return string page title
 */
function tgt_change_badge_name( $title ) {
    return 'Achievements';
}
add_filter( 'peepsogroups_template_title', 'tgt_change_badge_name' );

/**
 * Change “Badges” in profile nav
 * @param  array $links array of profile nav links
 * @return array array of profile nav links
 */
function tgt_change_badge_name_nav_profile( $links ) {
    if ( isset( $links['badges'] ) ) {
        $links['badges'] = array(
            'href' => 'badges',
            'label'=> __('Achievements', 'peepso-badgeos'),
            'icon' => 'ps-icon-star-empty'
        );
    }

    return $links;
}
add_filter( 'peepso_navigation_profile', 'tgt_change_badge_name_nav_profile', 25 );

/**
 * Show progress as a bar chart
 * @return string HTML content
 */
function tgt_progress_meter( $atts ) {
    $shortcode_attributes = shortcode_atts( array(
        'post_id'   => get_the_ID(),
    ), $atts );

    $query_args = array(
        'post_type'     => 'submission',
        'post_author'   => get_current_user_id(),
        'meta_query'    => array(
            array(
                'key'   => '_badgeos_submission_achievement_id',
                'value' => $shortcode_attributes['post_id'],
            ),
        ),
    );

    $query = new WP_Query( $query_args );

    ob_start();
    ?>
    <h2>My Progress</h2>
    <p><?php echo $query->found_posts; ?> out of 30 days</p>

    <div class="progress-frame">
        <div class="progress-bar" style="width: <?php echo ( $query->found_posts / 30 * 100 ); ?>%;"></div>
    </div>
    <?php
    return ob_get_clean();
}
add_shortcode( 'tgt_progress_meter', 'tgt_progress_meter' );

/**
 * Render shortcodes in achievements list
 *
 * By default, it shows the excerpt with unrendered shortcodes.
 *
 * @param  string  $content        HTML content
 * @param  integer $achievement_id WP post ID
 * @return string  HTML content
 */
function tgt_filter_out_progress_meter( $content, $achievement_id ) {
    return do_shortcode( $content );
}
add_filter( 'badgeos_render_achievement', 'tgt_filter_out_progress_meter', 10, 2 );
